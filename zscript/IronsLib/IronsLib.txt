// ------------------------------------------------------------
// Liberator Battle Rifle WITH NO BRIM
// ------------------------------------------------------------
const HDLD_ILIB="ilb";

class IronsLiberatorRifle:AutoReloadingThingy{
	default{
		//$Category "Weapons/Hideous Destructor"
		//$Title "Liberator"
		//$Sprite "IBFLB0"

		+hdweapon.fitsinbackpack
		weapon.slotnumber 6;
		weapon.slotpriority 2;
		weapon.kickback 20;
		weapon.selectionorder 27;
		inventory.pickupsound "misc/w_pkup";
		inventory.pickupmessage "You got the battle rifle! No dot sight on this one.";
		weapon.bobrangex 0.22;
		weapon.bobrangey 0.9;
		scale 0.7;
		obituary "%o was liberated by %k.";
		hdweapon.refid HDLD_ILIB;
		tag "Iron sights Liberator battle rifle";
		inventory.icon "IBFLB0";
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override void postbeginplay(){
		super.postbeginplay();
		if(weaponstatus[0]&ILBF_NOLAUNCHER){
			barrelwidth=0.7;
			barreldepth=1.2;
			weaponstatus[0]&=~(ILBF_GRENADEMODE|ILBF_GRENADELOADED);
		}else{
			barrelwidth=1;
			barreldepth=3;
		}
		if(weaponstatus[0]&ILBF_NOBULLPUP){
			barrellength=32;
			bfitsinbackpack=false;
		}else{
			barrellength=27;
		}
	}
	override double gunmass(){
		if(weaponstatus[0]&ILBF_NOBULLPUP){
			double howmuch=11;
			if(weaponstatus[0]&ILBF_NOLAUNCHER)return howmuch+weaponstatus[ILBS_MAG]*0.04;
			return howmuch+1.1+weaponstatus[ILBS_MAG]*0.05+(weaponstatus[0]&ILBF_GRENADELOADED?1.2:0.9);
		}else{
			double howmuch=9;
			if(weaponstatus[0]&ILBF_NOLAUNCHER)return howmuch+weaponstatus[ILBS_MAG]*0.04;
			return howmuch+1.+weaponstatus[ILBS_MAG]*0.04+(weaponstatus[0]&ILBF_GRENADELOADED?1.:0.6);
		}
	}
	override double weaponbulk(){
		double blx=(weaponstatus[0]&ILBF_NOBULLPUP)?120:100;
		if(!(weaponstatus[0]&ILBF_NOLAUNCHER)){
			blx+=28;
			if(weaponstatus[0]&ILBF_GRENADELOADED)blx+=ENC_ROCKETLOADED;
		}
		int mgg=weaponstatus[ILBS_MAG];
		return blx+(mgg<0?0:(ENC_776MAG_LOADED+mgg*ENC_776_LOADED));
	}
	override string,double getpickupsprite(){
		string spr;

		// A: -g +m +a
		// B: +g +m +a
		// C: -g -m +a
		// D: +g -m +a
		if(weaponstatus[0]&ILBF_NOLAUNCHER){
			if(weaponstatus[ILBS_MAG]<0)spr="C";
			else spr="A";
		}else{
			if(weaponstatus[ILBS_MAG]<0)spr="D";
			else spr="B";
		}

		// E: -g +m -a
		// F: +g +m -a
		// G: -g -m -a
		// H: +g -m -a
		if(weaponstatus[0]&ILBF_NOAUTO)spr=string.format("%c",spr.byteat(0)+4);

		return ((weaponstatus[0]&ILBF_NOBULLPUP)?"IBLL":"IBFL")..spr.."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("HD7mMag")));
			if(nextmagloaded>=30){
				sb.drawimage("RMAGNORM",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM);
			}else if(nextmagloaded<1){
				sb.drawimage("RMAGEMPTY",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.);
			}else sb.drawbar(
				"RMAGNORM","RMAGGREY",
				nextmagloaded,30,
				(-46,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawnum(hpl.countinv("HD7mMag"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			if(!(hdw.weaponstatus[0]&ILBF_NOLAUNCHER)){
				sb.drawimage("ROQPA0",(-62,-4),sb.DI_SCREEN_CENTER_BOTTOM,scale:(0.6,0.6));
				sb.drawnum(hpl.countinv("HDRocketAmmo"),-56,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			}
		}
		if(!(hdw.weaponstatus[0]&ILBF_NOAUTO)){
			string llba="RBRSA3A7";
			if(hdw.weaponstatus[0]&ILBF_FULLAUTO)llba="STFULAUT";
			sb.drawimage(
				llba,(-22,-10),
				sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TRANSLATABLE|sb.DI_ITEM_RIGHT
			);
		}
		if(hdw.weaponstatus[0]&ILBF_GRENADELOADED)sb.drawrect(-20,-15.6,4,2.6);
		int lod=max(hdw.weaponstatus[ILBS_MAG],0);
		sb.drawwepnum(lod,30);
		if(hdw.weaponstatus[ILBS_CHAMBER]==2){
			sb.drawrect(-19,-11,3,1);
			lod++;
		}
		if(hdw.weaponstatus[0]&ILBF_GRENADEMODE){
			int ab=hdw.airburst;
			sb.drawnum(ab,
				-30,-22,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TEXT_ALIGN_RIGHT,
				ab?Font.CR_WHITE:Font.CR_DARKGRAY
			);
			sb.drawrect(-34,-43+min(16,ab/10),4,1);
			sb.drawrect(-31,-42,1,16);
			sb.drawrect(-33,-42,1,16);
		}
	}
	override string gethelptext(){
		bool gl=!(weaponstatus[0]&ILBF_NOLAUNCHER);
		bool glmode=gl&&(weaponstatus[0]&ILBF_GRENADEMODE);
		return
		WEPHELP_FIRESHOOT
		..(gl?(WEPHELP_ALTFIRE..(glmode?("  Rifle mode\n"):("  GL mode\n"))):"")
		..WEPHELP_RELOAD.."  Reload mag\n"
		..WEPHELP_USE.."+"..WEPHELP_RELOAD.."  Reload chamber\n"
		..(gl?(WEPHELP_ALTRELOAD.."  Reload GL\n"):"")
		..(glmode?(WEPHELP_FIREMODE.."+"..WEPHELP_UPDOWN.."  Airburst\n")
			:(
			(WEPHELP_FIREMODE.."  Semi/Auto\n")
			..WEPHELP_ZOOM.."+"..WEPHELP_FIREMODE.."+"..WEPHELP_UPDOWN.."  Zoom\n"))
		..WEPHELP_MAGMANAGER
		..WEPHELP_UNLOAD.."  Unload "..(glmode?"GL\n":"magazine\n")
		..WEPHELP_UNLOAD.."+"..WEPHELP_USE.."  Assemble rounds"
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		if(hdw.weaponstatus[0]&ILBF_GRENADEMODE)sb.drawgrenadeladder(hdw.airburst,bob);
		else{
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=Screen.GetClipRect();
		sb.SetClipRect(
			-16+bob.x,-4+bob.y,32,16,
			sb.DI_SCREEN_CENTER
		);
		vector2 bobb=bob*2;
		bobb.y=clamp(bobb.y,-8,8);
		sb.drawimage(
			"frntsite",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9,scale:(1.6,2)
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"backsite",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			scale:(2,1.45)
		);
			if(scopeview){
				int scaledyoffset=60;
				int scaledwidth=72;
				double degree=hdw.weaponstatus[ILBS_ZOOM]*0.1;
				double deg=1/degree;
				int cx,cy,cw,ch;
				[cx,cy,cw,ch]=screen.GetClipRect();
				sb.SetClipRect(
					-36+bob.x,24+bob.y,scaledwidth,scaledwidth,
					sb.DI_SCREEN_CENTER
				);
				string reticle=
					hdw.weaponstatus[0]&ILBF_ALTRETICLE?"reticle2":"reticle1";
				texman.setcameratotexture(hpc,"HDXHCAM3",degree);
				sb.drawimage(
					"HDXHCAM3",(0,scaledyoffset)+bob,
					sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
					scale:(0.5,0.5)
				);
				if(hdw.weaponstatus[0]&ILBF_FRONTRETICLE){
					sb.drawimage(
						reticle,(0,scaledyoffset)+bob*5,
						sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
						scale:(1.6,1.6)*deg
					);
				}else{
					sb.drawimage(
						reticle,(0,scaledyoffset)+bob,
						sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
						scale:(0.52,0.52)
					);
				}
				sb.drawimage(
					"scophole",(0,scaledyoffset)+bob*5,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
					scale:(0.95,0.95)
				);
				screen.SetClipRect(cx,cy,cw,ch);
				sb.drawimage(
					"libscope",(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
				);
				sb.drawstring(
					sb.mAmountFont,string.format("%.1f",degree),
					(6+bob.x,95+bob.y),sb.DI_SCREEN_CENTER|sb.DI_TEXT_ALIGN_RIGHT,
					Font.CR_BLACK
				);
				sb.drawstring(
					sb.mAmountFont,string.format("%.1f",hdw.weaponstatus[ILBS_DROPADJUST]*0.1),
					(6+bob.x,17+bob.y),sb.DI_SCREEN_CENTER|sb.DI_TEXT_ALIGN_RIGHT,
					Font.CR_BLACK
				);
			}
		}
	}
	override void failedpickupunload(){
		failedpickupunloadmag(ILBS_MAG,"HD7mMag");
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("SevenMilAmmo"))owner.A_DropInventory("SevenMilAmmo",30);
			else{
				double angchange=(weaponstatus[0]&ILBF_NOLAUNCHER)?0:-10;
				if(angchange)owner.angle-=angchange;
				owner.A_DropInventory("HD7mMag",1);
				if(angchange){
					owner.angle+=angchange*2;
					owner.A_DropInventory("HDRocketAmmo",1);
					owner.angle-=angchange;
				}
			}
		}
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("SevenMilAmmo");
		owner.A_TakeInventory("SevenMilBrass");
		owner.A_TakeInventory("FourMilAmmo");
		owner.A_TakeInventory("HD7mMag");
		owner.A_GiveInventory("HD7mMag");
		if(!(weaponstatus[0]&ILBF_NOLAUNCHER)){
			owner.A_TakeInventory("DudRocketAmmo");
			owner.A_SetInventory("HDRocketAmmo",1);
		}
	}
	override void tick(){
		super.tick();
		drainheat(ILBS_HEAT,8);
	}
	action void A_Chamber(bool unloadonly=false){
		A_StartSound("weapons/libchamber",8,CHANF_OVERLAP);
		actor brsss=null;
		if(invoker.weaponstatus[ILBS_CHAMBER]==1){
			if(invoker.weaponstatus[0]&ILBF_NOBULLPUP){
				double cosp=cos(pitch);
				[cosp,brsss]=A_SpawnItemEx("HDSpent7mm",
					cosp*6,0,height-8-sin(pitch)*6,
					cosp*2,-1,2-sin(pitch),
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				brsss.vel+=vel;
				brsss.A_StartSound(brsss.bouncesound,volume:0.4);
			}else{
				int bss=invoker.weaponstatus[ILBS_BRASS];
				if(bss<random(1,7)){
					invoker.weaponstatus[ILBS_BRASS]++;
					A_StartSound("misc/casing",8,CHANF_OVERLAP);
				}else{
					double fc=max(pitch*0.01,5);
					double cosp=cos(pitch);
					[cosp,brsss]=A_SpawnItemEx("HDSpent7mm",
						cosp*12,0,height-8-sin(pitch)*12,
						cosp*fc,0.2*randompick(-1,1),-sin(pitch)*fc,
						0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
					);
					brsss.vel+=vel;
					brsss.A_StartSound(brsss.bouncesound,volume:0.4);
				}
			}
		}else if(invoker.weaponstatus[ILBS_CHAMBER]==2){
			double fc=max(pitch*0.01,5);
			double cosp=cos(pitch);
			[cosp,brsss]=A_SpawnItemEx("HDLoose7mm",
				cosp*12,0,height-8-sin(pitch)*12,
				cosp*fc,0.2*randompick(-1,1),-sin(pitch)*fc,
				0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
			);
			brsss.vel+=vel;
			brsss.A_StartSound(brsss.bouncesound,volume:0.4);
		}
		if(!unloadonly && invoker.weaponstatus[ILBS_MAG]>0){
			invoker.weaponstatus[ILBS_MAG]--;
			invoker.weaponstatus[ILBS_CHAMBER]=2;
		}else{
			invoker.weaponstatus[ILBS_CHAMBER]=0;
			if(brsss!=null)brsss.vel=vel+(cos(angle),sin(angle),-2);
		}
	}
	states{
	brasstube:
		TNT1 A 4{
			if(
				invoker.weaponstatus[ILBS_BRASS]>0
				&&(
					pitch>5
					||IsBusy(self)
				)
			){
				double fc=max(pitch*0.01,5);
				double cosp=cos(pitch);
				actor brsss;
				[cosp,brsss]=A_SpawnItemEx("HDSpent7mm",
					cosp*12,0,height-8-sin(pitch)*12,
					cosp*fc,0.2*randompick(-1,1),-sin(pitch)*fc,
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				brsss.vel+=vel;
				brsss.A_StartSound(brsss.bouncesound,volume:0.4);
				invoker.weaponstatus[ILBS_BRASS]--;
			}
		}wait;
	select0:
		ILIB  A 0{
			A_Overlay(776,"brasstube");
			invoker.weaponstatus[0]&=~ILBF_GRENADEMODE;
		}goto select0big;
	deselect0:
		ILIB  A 0{
			while(invoker.weaponstatus[ILBS_BRASS]>0){
				double cosp=cos(pitch);
				actor brsss;
				[cosp,brsss]=A_SpawnItemEx("HDSpent7mm",
					cosp*12,0,height-8-sin(pitch)*12,
					cosp*3,0.2*randompick(-1,1),-sin(pitch)*3,
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				brsss.vel+=vel;
				brsss.A_StartSound(brsss.bouncesound,volume:0.4);
				invoker.weaponstatus[ILBS_BRASS]--;
			}
		}goto deselect0big;
	ready:
		ILIB  A 1{
			if(pressingzoom()){
				if(player.cmd.buttons&BT_USE){
					A_ZoomAdjust(ILBS_DROPADJUST,0,600,BT_USE);
				}else if(invoker.weaponstatus[0]&ILBF_FRONTRETICLE)A_ZoomAdjust(ILBS_ZOOM,20,40);
				else A_ZoomAdjust(ILBS_ZOOM,6,70);
				A_WeaponReady(WRF_NONE);
			}else A_WeaponReady(WRF_ALL);
		}goto readyend;
	user3:
		---- A 0 A_JumpIf(!(invoker.weaponstatus[0]&ILBF_GRENADEMODE),1);
		goto super::user3;
		---- A 0 A_MagManager("HD7mMag");
		goto ready;

	fire:
		ILIB  A 0{
			if(
				invoker.weaponstatus[0]&ILBF_NOLAUNCHER
				||!(invoker.weaponstatus[0]&ILBF_GRENADEMODE)
			){
				setweaponstate("firegun");
			}else setweaponstate("firegrenade");
		}
	hold:
		ILIB  A 1{
			if(
				invoker.weaponstatus[0]&ILBF_GRENADEMODE
				||!(invoker.weaponstatus[0]&ILBF_FULLAUTO)
				||(invoker.weaponstatus[0]&ILBF_NOAUTO)
				||invoker.weaponstatus[ILBS_CHAMBER]!=2
			)setweaponstate("nope");
		}goto shoot;

	firegun:
		ILIB  A 1{
			if(invoker.weaponstatus[0]&ILBF_NOBULLPUP)A_SetTics(0);
			else if(invoker.weaponstatus[0]&ILBF_FULLAUTO)A_SetTics(2);
		}
	shoot:
		ILIB  A 1{
			if(invoker.weaponstatus[ILBS_CHAMBER]==2)A_Gunflash();
			else setweaponstate("chamber_manual");
			A_WeaponReady(WRF_NONE);
		}
		ILIB  B 1 A_Chamber();
		ILIB  A 0 A_Refire();
		goto nope;
	flash:
		ILIF A 1 bright{
			A_Light1();
			A_StartSound("weapons/bigrifle",CHAN_WEAPON);

			HDBulletActor.FireBullet(self,"HDB_776",
				aimoffy:(-1./600.)*invoker.weaponstatus[ILBS_DROPADJUST]
			);
/*
			actor p=spawn("HDBullet776",pos+(0,0,height-6),ALLOW_REPLACE);
			p.target=self;p.angle=angle;p.pitch=pitch;
			p.vel+=self.vel;
			p.pitch-=(1./600.)*invoker.weaponstatus[ILBS_DROPADJUST];
*/
			if(invoker.weaponstatus[0]&ILBF_NOBULLPUP){
				HDFlashAlpha(16);
				A_ZoomRecoil(0.90);
				A_MuzzleClimb(
					0,0,
					-0.07,-0.14,
					-frandom(0.3,0.6),-frandom(1.,1.4),
					-frandom(0.2,0.4),-frandom(1.,1.4)
				);
			}else{
				HDFlashAlpha(32);
				A_ZoomRecoil(0.95);
				A_MuzzleClimb(
					0,0,
					-0.2,-0.4,
					-frandom(0.5,0.9),-frandom(1.7,2.1),
					-frandom(0.5,0.9),-frandom(1.7,2.1)
				);
			}

			invoker.weaponstatus[ILBS_CHAMBER]=1;
			invoker.weaponstatus[ILBS_HEAT]+=2;
			A_AlertMonsters();
		}
		goto lightdone;
	chamber_manual:
		ILIB  A 1 offset(-1,34){
			if(
				invoker.weaponstatus[ILBS_CHAMBER]==2
				||invoker.weaponstatus[ILBS_MAG]<1
			)setweaponstate("nope");
		}
		ILIB  B 1 offset(-2,36)A_Chamber();
		ILIB  B 1 offset(-2,38);
		ILIB  A 1 offset(-1,34);
		goto nope;


	firemode:
		---- A 0{
			if(invoker.weaponstatus[0]&ILBF_GRENADEMODE)setweaponstate("abadjust");
			else if(!(invoker.weaponstatus[0]&ILBF_NOAUTO))invoker.weaponstatus[0]^=ILBF_FULLAUTO;
		}goto nope;


	unloadchamber:
		ILIB  B 1 offset(-1,34){
			if(
				invoker.weaponstatus[ILBS_CHAMBER]<1
			)setweaponstate("nope");
		}
		ILIB  B 1 offset(-2,36)A_Chamber(true);
		ILIB  B 1 offset(-2,38);
		ILIB  A 1 offset(-1,34);
		goto nope;

	loadchamber:
		ILIB  A 0 A_JumpIf(invoker.weaponstatus[ILBS_CHAMBER]>0,"nope");
		ILIB  A 0 A_JumpIf(!countinv("SevenMilAmmo"),"nope");
		ILIB  A 1 offset(0,34) A_StartSound("weapons/pocket",9);
		ILIB  A 2 offset(2,36);
		ILIB  B 8 offset(5,40);
		ILIB  B 8 offset(7,44);
		ILIB  B 8 offset(6,43);
		ILIB  B 10 offset(4,39){
			if(countinv("SevenMilAmmo")){
				A_TakeInventory("SevenMilAmmo",1,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[ILBS_CHAMBER]=2;
				A_StartSound("weapons/libchamber2",8);
				A_StartSound("weapons/libchamber2a",8,CHANF_OVERLAP,0.7);
			}else A_SetTics(4);
		}
		ILIB  B 7 offset(5,37);
		ILIB  B 1 offset(2,36);
		ILIB  A 1 offset(0,34);
		goto readyend;

	user4:
	unload:
		---- A 1 A_CheckChug(pressinguse()); //DO NOT set this frame to zero
		ILIB  A 0{
			invoker.weaponstatus[0]|=ILBF_JUSTUNLOAD;
			if(
				invoker.weaponstatus[0]&ILBF_GRENADEMODE
			){
				return resolvestate("unloadgrenade");
			}else if(
				invoker.weaponstatus[ILBS_MAG]>=0  
			){
				return resolvestate("unmag");
			}else if(
				invoker.weaponstatus[ILBS_CHAMBER]>0  
			){
				return resolvestate("unloadchamber");
			}
			return resolvestate("nope");
		}
	reload:
		ILIB  A 0{
			int inmag=invoker.weaponstatus[ILBS_MAG];
			invoker.weaponstatus[0]&=~ILBF_JUSTUNLOAD;
			if(
				//no point reloading
				inmag>=30
				||(
					//no mags to load and can't directly load chamber
					!countinv("HD7mMag")
					&&(
						inmag>=0
						||invoker.weaponstatus[ILBS_CHAMBER]>0
						||!countinv("SevenMilAmmo")
					)
				)
			)return resolvestate("nope");
			else if(
				//no mag, empty chamber, have loose rounds
				inmag<0
				&&invoker.weaponstatus[ILBS_CHAMBER]<1
				&&countinv("SevenMilAmmo")
				&&(
					pressinguse()
					||HDMagAmmo.NothingLoaded(self,"HD7mMag")
				)
			)return resolvestate("loadchamber");
			else if(
				invoker.weaponstatus[ILBS_MAG]>0  
			){
				//if full mag and unchambered, chamber
				if(
					invoker.weaponstatus[ILBS_MAG]>=30  
					&&invoker.weaponstatus[ILBS_CHAMBER]!=2
				){
					return resolvestate("chamber_manual");
				}				
			}return resolvestate("unmag");
		}

	unmag:
		ILIB  A 1 offset(0,34);
		ILIB  A 1 offset(2,36);
		ILIB  B 1 offset(4,40);
		ILIB  B 2 offset(8,42){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/rifleclick2",8);
		}
		ILIB  B 4 offset(14,46){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound ("weapons/rifleload",8,CHANF_OVERLAP);
		}
		ILIB  B 0{
			int magamt=invoker.weaponstatus[ILBS_MAG];
			if(magamt<0){setweaponstate("magout");return;}
			invoker.weaponstatus[ILBS_MAG]=-1;
			if(
				!PressingReload()
				&&!PressingUnload()
			){
				HDMagAmmo.SpawnMag(self,"HD7mMag",magamt);
				setweaponstate("magout");
			}else{
				HDMagAmmo.GiveMag(self,"HD7mMag",magamt);
				setweaponstate("pocketmag");
			}
		}
	pocketmag:
		ILIB  B 7 offset(12,52)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		ILIB  B 0 A_StartSound("weapons/pocket",9);
		ILIB  BB 7 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		ILIB  B 0{
		}goto magout;
	magout:
		ILIB  B 4{
			invoker.weaponstatus[ILBS_MAG]=-1;
			if(invoker.weaponstatus[0]&ILBF_JUSTUNLOAD)setweaponstate("reloaddone");
		}goto loadmag;


	loadmag:
		ILIB  B 0 A_StartSound("weapons/pocket",9);
		ILIB  BB 7 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.4),frandom(-0.2,0.8));
		ILIB  B 6 offset(12,52){
			let mmm=hdmagammo(findinventory("HD7mMag"));
			if(mmm){
				invoker.weaponstatus[ILBS_MAG]=mmm.TakeMag(true);
				A_StartSound("weapons/rifleclick",8);
				A_StartSound("weapons/rifleload",8,CHANF_OVERLAP);
			}
		}
		ILIB  B 2 offset(8,46) A_StartSound("weapons/rifleclick2",8,CHANF_OVERLAP);
		goto reloaddone;

	reloaddone:
		ILIB  B 1 offset (4,40);
		ILIB  A 1 offset (2,34);
		goto chamber_manual;


	altfire:
		ILIB  A 1 offset(0,34){
			if(invoker.weaponstatus[0]&ILBF_NOLAUNCHER){
				invoker.weaponstatus[0]&=~(ILBF_GRENADEMODE|ILBF_GRENADELOADED);
				setweaponstate("nope");
			}else invoker.airburst=0;
		}
		ILIB  A 1 offset(2,36);
		ILIB  B 1 offset(4,40);
		ILIB  B 1 offset(2,36);
		ILIB  A 1 offset(0,34);
		ILIB  A 0{
			invoker.weaponstatus[0]^=ILBF_GRENADEMODE;
			A_SetHelpText();
			A_Refire();
		}goto ready;
	althold:
		ILIB  A 0;
		goto nope;


	firegrenade:
		ILIB  B 2{
			if(invoker.weaponstatus[0]&ILBF_GRENADELOADED){
				A_FireHDGL();
				invoker.weaponstatus[0]&=~ILBF_GRENADELOADED;
				if(invoker.weaponstatus[0]&ILBF_NOBULLPUP){
					A_ZoomRecoil(0.99);
					A_MuzzleClimb(
						0,0,
						-0.8,-2.,
						-0.4,-1.
					);
				}else{
					A_ZoomRecoil(0.95);
					A_MuzzleClimb(
						0,0,
						-1.2,-3.,
						-0.6,-1.4
					);
				}
			}else setweaponstate("nope");
		}
		ILIB  B 2;
		ILIB  A 0 A_Refire("nope");
		goto ready;
	altreload:
		ILIB  A 0{
			if(!(invoker.weaponstatus[0]&ILBF_NOLAUNCHER)){
				invoker.weaponstatus[0]&=~ILBF_JUSTUNLOAD;
				setweaponstate("unloadgrenade");
			}
		}goto nope;
	unloadgrenade:
		ILIB  A 1 offset(0,34){
			A_SetCrosshair(21);
			if(
				(
					//just unloading but no grenade
					invoker.weaponstatus[0]&ILBF_JUSTUNLOAD
					&&!(invoker.weaponstatus[0]&ILBF_GRENADELOADED)
				)||(
					//reloading but no ammo or already loaded
					!(invoker.weaponstatus[0]&ILBF_JUSTUNLOAD)
					&&(
						!countinv("HDRocketAmmo")
						||invoker.weaponstatus[0]&ILBF_GRENADELOADED
					)
				)
			){
				setweaponstate("nope");
			}
		}
		ILIB  A 1 offset(-5,40);
		ILIB  A 1 offset(-10,50);
		ILIB  A 1 offset(-15,56);
		ILIB  A 4 offset(-14,54){
			A_StartSound("weapons/pocket",9);
			A_StartSound("weapons/grenopen",8);
		}
		ILIB  A 3 offset(-16,56){
			if(invoker.weaponstatus[0]&ILBF_GRENADELOADED){
				if(
					(PressingReload()||PressingUnload())
					&&!A_JumpIfInventory("HDRocketAmmo",0,"null")
				){
					A_GiveInventory("HDRocketAmmo");
					A_StartSound("weapons/pocket",9);
					A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
					A_SetTics(6);
				}else A_SpawnItemEx("HDRocketAmmo",
					cos(pitch)*12,0,height-10-12*sin(pitch),
					vel.x,vel.y,vel.z,
					0,SXF_SETTARGET|SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				invoker.weaponstatus[0]&=~ILBF_GRENADELOADED;
			}
		}
		ILIB  A 0{
			if(invoker.weaponstatus[0]&ILBF_JUSTUNLOAD)setweaponstate("altreloaddone");
		}
		ILIB  AA 8 offset(-16,56)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		ILIB  A 18 offset(-14,54)A_StartSound("weapons/grenreload",8);
		ILIB  B 4 offset(-12,50){
			A_StartSound("weapons/grenopen",8);
			A_TakeInventory("HDRocketAmmo",1,TIF_NOTAKEINFINITE);
			invoker.weaponstatus[0]|=ILBF_GRENADELOADED;
		}
	altreloaddone:
		ILIB  A 1 offset(-15,56);
		ILIB  A 1 offset(-10,50);
		ILIB  A 1 offset(-5,40);
		ILIB  A 1 offset(0,34);
		goto nope;

	spawn:
		IBFL ABCDEFGH -1 nodelay{
			if(invoker.weaponstatus[0]&ILBF_NOBULLPUP){
				sprite=getspriteindex("IBLLA0");
			}
			// A: -g +m +a
			// B: +g +m +a
			// C: -g -m +a
			// D: +g -m +a
			if(invoker.weaponstatus[0]&ILBF_NOLAUNCHER){
				if(invoker.weaponstatus[ILBS_MAG]<0)frame=2;
				else frame=0;
			}else{
				if(invoker.weaponstatus[ILBS_MAG]<0)frame=3;
				else frame=1;
			}

			// E: -g +m -a
			// F: +g +m -a
			// G: -g -m -a
			// H: +g -m -a
			if(invoker.weaponstatus[0]&ILBF_NOAUTO)frame+=4;

			if(
				invoker.makinground
				&&invoker.brass>0
				&&invoker.powders>=3
			)setstatelabel("chug");
		}
		IBLL ABCDEFGH -1;
		stop;
	}
	override void InitializeWepStats(bool idfa){
		if(!(weaponstatus[0]&ILBF_NOLAUNCHER))weaponstatus[0]|=ILBF_GRENADELOADED;
		weaponstatus[ILBS_MAG]=30;
		weaponstatus[ILBS_CHAMBER]=2;
		if(!idfa&&!owner){
			weaponstatus[ILBS_ZOOM]=30;
			weaponstatus[ILBS_HEAT]=0;
			weaponstatus[ILBS_DROPADJUST]=160;
		}
	}
	override void loadoutconfigure(string input){
		int nogl=getloadoutvar(input,"nogl",1);
		//disable launchers if rocket grenades blacklisted
		string blacklist=hd_blacklist;
		if(blacklist.IndexOf(HDLD_BLOOPER)>=0)nogl=1;
		if(!nogl){
			weaponstatus[0]&=~ILBF_NOLAUNCHER;
		}else if(nogl>0){
			weaponstatus[0]|=ILBF_NOLAUNCHER;
			weaponstatus[0]&=~ILBF_GRENADELOADED;
		}
		if(!(weaponstatus[0]&ILBF_NOLAUNCHER))weaponstatus[0]|=ILBF_GRENADELOADED;

		int nobp=getloadoutvar(input,"nobp",1);
		if(!nobp)weaponstatus[0]&=~ILBF_NOBULLPUP;
		else if(nobp>0)weaponstatus[0]|=ILBF_NOBULLPUP;
		if(weaponstatus[0]&ILBF_NOBULLPUP)bfitsinbackpack=false;
		else bfitsinbackpack=true;

		int altreticle=getloadoutvar(input,"altreticle",1);
		if(!altreticle)weaponstatus[0]&=~ILBF_ALTRETICLE;
		else if(altreticle>0)weaponstatus[0]|=ILBF_ALTRETICLE;

		int frontreticle=getloadoutvar(input,"frontreticle",1);
		if(!frontreticle)weaponstatus[0]&=~ILBF_FRONTRETICLE;
		else if(frontreticle>0)weaponstatus[0]|=ILBF_FRONTRETICLE;

		int bulletdrop=getloadoutvar(input,"bulletdrop",3);
		if(bulletdrop>=0)weaponstatus[ILBS_DROPADJUST]=clamp(bulletdrop,0,600);

		int zoom=getloadoutvar(input,"zoom",3);
		if(zoom>=0)weaponstatus[ILBS_ZOOM]=
			(weaponstatus[0]&ILBF_FRONTRETICLE)?
			clamp(zoom,20,40):
			clamp(zoom,6,70);

		int firemode=getloadoutvar(input,"firemode",1);
		if(firemode>0)weaponstatus[0]|=ILBF_FULLAUTO;
		else weaponstatus[0]&=~ILBF_FULLAUTO;

		int semi=getloadoutvar(input,"semi",1);
		if(semi>0){
			weaponstatus[0]|=ILBF_NOAUTO;
			weaponstatus[0]&=~ILBF_FULLAUTO;
		}else weaponstatus[0]&=~ILBF_NOAUTO;
	}
}
enum ironsliberatorstatus{
	ILBF_FULLAUTO=1,
	ILBF_JUSTUNLOAD=2,
	ILBF_GRENADELOADED=4,
	ILBF_NOLAUNCHER=8,
	ILBF_FRONTRETICLE=32,
	ILBF_ALTRETICLE=64,
	ILBF_GRENADEMODE=128,
	ILBF_UNLOADONLY=256,
	ILBF_NOBULLPUP=512,
	ILBF_NOAUTO=1024,

	ILBS_FLAGS=0,
	ILBS_CHAMBER=1,
	ILBS_MAG=2, //-1 is ampty
	ILBS_ZOOM=3,
	ILBS_HEAT=4,
	ILBS_BRASS=5,
	ILBS_AIRBURST=6,
	ILBS_DROPADJUST=7,
};

class IronsLiberatorRandom:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let lll=LiberatorRifle(spawn("IronsLiberatorRifle",pos,ALLOW_REPLACE));
			if(!lll)return;
			lll.special=special;
			lll.vel=vel;
			for(int i=0;i<5;i++)lll.args[i]=args[i];
			if(!random(0,2))lll.weaponstatus[0]|=ILBF_FRONTRETICLE;
			if(!random(0,2))lll.weaponstatus[0]|=ILBF_ALTRETICLE;
			if(!random(0,2))lll.weaponstatus[0]|=ILBF_NOLAUNCHER;
			if(!random(0,3))lll.weaponstatus[0]|=ILBF_NOBULLPUP;
			if(!random(0,5))lll.weaponstatus[0]|=ILBF_NOAUTO;

			if(lll.weaponstatus[0]&ILBF_NOLAUNCHER){
				spawn("HD7mMag",pos+(7,0,0),ALLOW_REPLACE);
				spawn("HD7mMag",pos+(5,0,0),ALLOW_REPLACE);
			}else{
				spawn("HDRocketAmmo",pos+(10,0,0),ALLOW_REPLACE);
				spawn("HDRocketAmmo",pos+(8,0,0),ALLOW_REPLACE);
				spawn("HD7mMag",pos+(5,0,0),ALLOW_REPLACE);
			}
		}stop;
	}
}
