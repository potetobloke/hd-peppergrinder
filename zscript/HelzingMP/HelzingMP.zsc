// ------------------------------------------------------------
// L&M Helzing 355 Magnum Pistol
// ------------------------------------------------------------
/*  One of the later models of the later models of L&M personal firearms, the
Helzing-Model Magnum performs as a reliable double action magnum pistol with
an interesting quirk forced into the weapon's production in the form of a
pre-installed bizzare underbarrel attatchment.

  After an intense fever dream, it's designer was rumored to have "Peered into
Hell itself" and designed the gun to contain an angled tap to hold and siphon
liquid bottles that may be drip fed directly into the chamber and directly over
the bullet. Despite protests and better sense, the attatchment was approved and
personally funded by higher leads within L&M.

  This completely useless attatchment became notorious after an incedent of UAC
marines loading the liquid loader with a foul smelling chemical known as
"532-LIQUID ASS" and firing blanks at ranges.
  Despite the negative reputation, this attatchment rose back into popularity
when a former priest in the WING Unit managed to load it with holy water and
use it to fire cartridges that could hit demons like a 10 gauge slug.*/


const HDLD_HELLMP="Hmp";
class HDHelzing:HDHandgun{
	default{
		+hdweapon.fitsinbackpack
		+hdweapon.reverseguninertia
		scale 0.63;
		weapon.selectionorder 50;
		weapon.slotnumber 2;
		weapon.slotpriority 2;
		weapon.kickback 30;
		weapon.bobrangex 0.1;
		weapon.bobrangey 0.6;
		weapon.bobspeed 2.5;
		weapon.bobstyle "normal";
		obituary "%o got capped by %k's Helzing.";
		inventory.pickupmessage "You got the Helzing Magnum Pistol!";
		tag "Helzing Magnum";
		hdweapon.refid HDLD_HELLMP;
		hdweapon.barrelsize 10,0.3,0.5;
		inventory.maxamount 3;
	}
	override double weaponbulk(){
		int mgg=weaponstatus[HMPS_MAG];
		return 30+(mgg<0?0:(ENC_355MAG_EMPTY+mgg*ENC_355))+(weaponstatus[0]&HMPF_BOTTLE?ENC_BLUEPOTION:0);
	}
	override double gunmass(){
		return 10;
	}
	override void failedpickupunload(){
		failedpickupunloadmag(HMPS_MAG,"HD355Mag");
	}
	override string,double getpickupsprite(){
		string spr;
		if(weaponstatus[0]&HMPF_BOTTLE)spr="B";
		else spr="A";
		return "HMPT"..spr.."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("HD355Mag")));
			if(nextmagloaded>=12){
				sb.drawimage("3MAGB0",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(1,1));
			}else if(nextmagloaded<1){
				sb.drawimage("3MAGA0",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.,scale:(1,1));
			}else sb.drawbar(
				"3MAGB0","3MAGC0",
				nextmagloaded,12,
				(-46,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawnum(hpl.countinv("HD355Mag"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		sb.drawwepcounter(hdw.weaponstatus[0]&HMPF_BLUEUSE,
			-35,-10,"","BLHD2"
		);
		sb.drawwepnum(hdw.weaponstatus[HMPS_MAG],12);
		if(hdw.weaponstatus[0]&HMPF_BOTTLE)sb.drawwepnum(hdw.weaponstatus[HMPS_BLUE],12,posy:-3);
		if(hdw.weaponstatus[HMPS_CHAMBER]==2)sb.drawrect(-19,-11,3,1);
	}
	override string gethelptext(){
		bool blue=(weaponstatus[0]&HMPF_BLUEUSE);
		bool hasblu=(weaponstatus[0]&HMPF_BOTTLE);
		return
		WEPHELP_FIRE..(hasblu?(blue?(" Fire Consecrated Round\n"):(" Shoot\n")):" Shoot\n")
		..WEPHELP_RELOAD..(blue?" Reload Bottle":" Reload").."\n"
		..WEPHELP_UNLOAD..(blue?" Unload Bottle\n":" Unload\n")
		..WEPHELP_FIREMODE..(blue?" Close Valve\n":" Open Valve\n")
		..WEPHELP_ALTRELOAD.." Quick-Swap (if available)\n"
		..WEPHELP_MAGMANAGER
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		vector2 scc;
		vector2 bobb=bob*1.6;
		if(hpl.player.getpsprite(PSP_WEAPON).frame>=2){
			sb.SetClipRect(
				-10+bob.x,-5+bob.y,20,14,
				sb.DI_SCREEN_CENTER
			);
			scc=(0.7,0.8);
			bobb.y=clamp(bobb.y*1.1-3,-10,10);
		}else if(weaponstatus[0]&HMPF_BLUEUSE){
			sb.SetClipRect(
				-8+bob.x,-4+bob.y,16,-10,
				sb.DI_SCREEN_CENTER
			);
			scc=(0.9,0.9);
			bobb.y=clamp(bobb.y,-8,8);
		}else{
			sb.SetClipRect(
				-8+bob.x,-4+bob.y,16,10,
				sb.DI_SCREEN_CENTER
			);
			scc=(0.6,0.6);
			bobb.y=clamp(bobb.y,-8,8);
		}
		if(weaponstatus[0]&HMPF_BLUEUSE)
		sb.drawimage(
			"rusorth",(0,-5)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9,scale:scc
		);
		else sb.drawimage(
			"frntsite",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9,scale:scc
		);
		sb.SetClipRect(cx,cy,cw,ch);
		if(weaponstatus[0]&HMPF_BLUEUSE)
		sb.drawimage(
			"hzguide",(0,-3)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			scale:scc
		);
		else sb.drawimage(
			"backsite",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			scale:scc
		);
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("HDRevolverAmmo"))owner.A_DropInventory("HDRevolverAmmo",amt*12);
			else owner.A_DropInventory("HD355Mag",amt);
		}
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("HDRevolverAmmo");
		owner.A_TakeInventory("HD355Mag");
		owner.A_GiveInventory("HD355Mag");
	}
	action void A_CheckPistolHand(){
		if(invoker.wronghand)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("HSACA0");
	}
	states{
	select0:
		HSAC A 0{
			if(!countinv("NulledWeapon"))invoker.wronghand=false;
			A_TakeInventory("NulledWeapon");
			A_CheckPistolHand();
		}
		#### A 0 A_JumpIf(invoker.weaponstatus[HMPS_CHAMBER]>0,2);
		#### E 0;
		---- A 1 A_Raise();
		---- A 1 A_Raise(30);
		---- A 1 A_Raise(30);
		---- A 1 A_Raise(24);
		---- A 1 A_Raise(18);
		wait;
	deselect0:
		HSAC A 0 A_CheckPistolHand();
		#### A 0 A_JumpIf(invoker.weaponstatus[HMPS_CHAMBER]>0,2);
		#### E 0;
		---- AAA 1 A_Lower();
		---- A 1 A_Lower(18);
		---- A 1 A_Lower(24);
		---- A 1 A_Lower(30);
		wait;

	ready:
		HSAC A 0 A_CheckPistolHand();
		#### A 1{
			if(invoker.weaponstatus[0]&HMPF_BLUEUSE){
				if(PressingFire()){
					setweaponstate("fire");
				}
				if(PressingReload()){
					setweaponstate("reblue");
				}
				if(PressingUnload()){
					invoker.weaponstatus[0]|=HMPF_JUSTUNLOAD;
					setweaponstate("unblue");
				}
			}else{
				if(PressingReload()){
					setweaponstate("reload");
				}
				if(PressingUnload()){
					setweaponstate("unload");
				}
				if(PressingFire()){
					setweaponstate("Fire");
				}
			}
		}
		#### A 0 A_JumpIf(invoker.weaponstatus[HMPS_CHAMBER]>0,2);
		#### E 0;
		#### # 0 A_SetCrosshair(21);
		#### # 0 A_WeaponReady((WRF_ALL|WRF_NOFIRE)&~WRF_ALLOWUSER4&~WRF_ALLOWRELOAD);
		goto readyend;
	user3:
		---- A 0 A_MagManager("HD355Mag");
		goto ready;
	firemode:
		---- A 0{
			invoker.weaponstatus[0]^=HMPF_BLUEUSE;
			A_SetHelpText();
		}goto nope;
	altfire:
		---- A 0{
			invoker.weaponstatus[0]&=~HMPF_JUSTUNLOAD;
			if(
				invoker.weaponstatus[HMPS_CHAMBER]!=2
				&&invoker.weaponstatus[HMPS_MAG]>0
			)setweaponstate("chamber_manual");
		}goto nope;
	chamber_manual:
		---- A 0 A_JumpIf(
			!(invoker.weaponstatus[0]&HMPF_JUSTUNLOAD)
			&&(
				invoker.weaponstatus[HMPS_CHAMBER]==2
				||invoker.weaponstatus[HMPS_MAG]<1
			)
			,"nope"
		);
		#### E 3 offset(0,34);
		#### D 4 offset(0,37){
			A_MuzzleClimb(frandom(0.4,0.5),-frandom(0.6,0.8));
			A_StartSound("weapons/pischamber2",8);
			int psch=invoker.weaponstatus[HMPS_CHAMBER];
			invoker.weaponstatus[HMPS_CHAMBER]=0;
			if(psch==2){
				A_SpawnItemEx("HDRevolverAmmo",cos(pitch*12),0,height-9-sin(pitch)*12,1,2,3,0);
			}else if(psch==1){
				A_SpawnItemEx("HDSpent355",
					cos(pitch)*12,0,height-9-sin(pitch)*12,
					vel.x,vel.y,vel.z,
					0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}
			if(invoker.weaponstatus[HMPS_MAG]>0){
				invoker.weaponstatus[HMPS_CHAMBER]=2;
				invoker.weaponstatus[HMPS_MAG]--;
			}
		}
		#### E 3 offset(0,35);
		goto nope;
	althold:
	hold:
		goto nope;
	fire:
		---- A 0{
			invoker.weaponstatus[0]&=~HMPF_JUSTUNLOAD;
			if(invoker.weaponstatus[HMPS_CHAMBER]==2){
				if(invoker.weaponstatus[0]&HMPF_BLUEUSE&&invoker.weaponstatus[HMPS_BLUE]>0)setweaponstate("shootB");
				else setweaponstate("shootA");}
			else if(invoker.weaponstatus[HMPS_MAG]>0)setweaponstate("chamber_manual");
		}goto nope;
	shootA:
		#### A 0{if(invoker.weaponstatus[HMPS_CHAMBER]==2)A_GunFlash();}
		#### BC 1;
		goto shoot;
	shootB:
		#### A 0{if(invoker.weaponstatus[HMPS_CHAMBER]==2)A_GunFlash();}
		#### LM 1;
		goto shoot;
	shoot:
		#### DE 1{
			if(hdplayerpawn(self)){
				hdplayerpawn(self).gunbraced=false;
				A_GiveInventory("IsMoving",1);
			}
			A_MuzzleClimb(
				-frandom(0.4,0.5),-frandom(0.6,0.8),
				frandom(0.2,0.3),frandom(0.3,0.4));
		}
		#### A 0{
			A_SpawnItemEx("HDSpent355",
				cos(pitch)*12,0,height-9-sin(pitch)*12,
				vel.x,vel.y,vel.z,
				0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
			);
			invoker.weaponstatus[HMPS_CHAMBER]=0;
			if(invoker.weaponstatus[HMPS_MAG]<1){
				A_StartSound("weapons/pistoldry",8,CHANF_OVERLAP,0.9);
				setweaponstate("nope");
			}
		}
		#### A 1{
			A_WeaponReady(WRF_NOFIRE);
			invoker.weaponstatus[HMPS_CHAMBER]=2;
			invoker.weaponstatus[HMPS_MAG]--;
			if(
				(invoker.weaponstatus[0]&(HMPF_BLUEUSE))
			){
				invoker.weaponstatus[HMPS_BLUE]--;
				let pnr=HDPlayerPawn(self);
				if(
					pnr&&countinv("IsMoving")
					&&pnr.fatigue<12
				)pnr.fatigue++;
				A_GiveInventory("IsMoving",5);
				A_Refire();
			}else A_Refire();
		}goto ready;
	flash:
		HSAC A 0 A_JumpIf(invoker.wronghand,2);
		HSAC A 0;
		---- A 0 bright{
			HDFlashAlpha(64);
			A_Light1();
			bool blue=(invoker.weaponstatus[0]&HMPF_BLUEUSE&&invoker.weaponstatus[HMPS_BLUE]>0);
			let bbb=HDBulletActor.FireBullet(self,blue?"HDB_355HOLY":"HDB_355",spread:1.,speedfactor:frandom(0.99,1.01));
			if(
				frandom(0,ceilingz-floorz)<bbb.speed*0.3
			)A_AlertMonsters(256);

			invoker.weaponstatus[HMPS_CHAMBER]=1;
			A_ZoomRecoil(0.995);
			A_StartSound("weapons/deinoblast1",CHAN_WEAPON,CHANF_OVERLAP,1.5);
			A_StartSound("weapons/deinoblast2",CHAN_WEAPON,CHANF_OVERLAP,0.4);
			if(blue){
				A_MuzzleClimb(-frandom(1.0,1.8),-frandom(3.8,4.2));;
				A_StartSound("weapons/hmpholy",CHAN_WEAPON,CHANF_OVERLAP,0.6);
				A_StartSound("weapons/deinoblast1",CHAN_WEAPON,CHANF_OVERLAP,0.5);
				A_StartSound("weapons/deinoblast2",CHAN_WEAPON,CHANF_OVERLAP,0.8);
			}
		}
		---- A 0 A_Light0();
		stop;
	unload:
		---- A 0{
			invoker.weaponstatus[0]|=HMPF_JUSTUNLOAD;
			if(invoker.weaponstatus[HMPS_MAG]>=0)setweaponstate("unmag");
		}goto chamber_manual;
	loadchamber:
		---- A 0 A_JumpIf(invoker.weaponstatus[HMPS_CHAMBER]>0,"nope");
		---- A 1 offset(0,36) A_StartSound("weapons/pocket",9);
		---- A 1 offset(2,40);
		---- A 1 offset(2,50);
		---- A 1 offset(3,60);
		---- A 2 offset(5,90);
		---- A 2 offset(7,80);
		---- A 2 offset(10,90);
		#### E 2 offset(8,96);
		#### E 3 offset(6,88){
			if(countinv("HDRevolverAmmo")){
				A_TakeInventory("HDRevolverAmmo",1,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[HMPS_CHAMBER]=2;
				A_StartSound("weapons/pischamber1",8);
			}
		}
		#### E 2 offset(5,76);
		#### E 1 offset(4,64);
		#### E 1 offset(3,56);
		#### E 1 offset(2,48);
		#### E 2 offset(1,38);
		#### E 3 offset(0,34);
		goto readyend;
	reload:
		HSAC A 0{
			invoker.weaponstatus[0]&=~HMPF_JUSTUNLOAD;
			bool nomags=HDMagAmmo.NothingLoaded(self,"HD355Mag");
			if(invoker.weaponstatus[HMPS_MAG]>=12)setweaponstate("nope");
			else if(
				invoker.weaponstatus[HMPS_MAG]<1
				&&(
					pressinguse()
					||nomags
				)
			){
				if(
					countinv("HDRevolverAmmo")
				)setweaponstate("loadchamber");
				else setweaponstate("nope");
			}else if(nomags)setweaponstate("nope");
		}goto unmag;
	unmag:
		HSAC A 1 A_SetCrosshair(21);
		#### H 1 ;
		#### I 2 ;
		#### J 3 A_StartSound("weapons/pismagclick",8,CHANF_OVERLAP);
		---- J 0{
			int pmg=invoker.weaponstatus[HMPS_MAG];
			invoker.weaponstatus[HMPS_MAG]=-1;
			if(pmg<0)setweaponstate("magout");
			else if(
				(!PressingUnload()&&!PressingReload())
				||A_JumpIfInventory("HD355Mag",0,"null")
			){
				HDMagAmmo.SpawnMag(self,"HD355Mag",pmg);
				setweaponstate("magout");
			}
			else{
				HDMagAmmo.GiveMag(self,"HD355Mag",pmg);
				A_StartSound("weapons/pocket",9);
				setweaponstate("pocketmag");
			}
		}
	pocketmag:
		#### JJK 5 A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		goto magout;
	magout:
		#### J 0{
			bool unl=invoker.weaponstatus[0]&HMPF_JUSTUNLOAD;
			if(!unl)setweaponstate("loadmag");
			}
		#### JIH 5 A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		---- A 3;
		goto reloadend;
	loadmag:
		#### K 4 A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		#### K 0 A_StartSound("weapons/pocket",9);
		#### JIH 5 A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		---- A 3;
		---- A 0{
			let mmm=hdmagammo(findinventory("HD355Mag"));
			if(mmm){
				invoker.weaponstatus[HMPS_MAG]=mmm.TakeMag(true);
				A_StartSound("weapons/pismagclick",8);
			}
		}goto reloadend;
	reloadend:
		#### A 2 offset(3,46);
		---- A 1 offset(2,42);
		---- A 1 offset(2,38);
		---- A 1 offset(1,34);
		---- A 0 A_JumpIf(!(invoker.weaponstatus[0]&HMPF_JUSTUNLOAD),"chamber_manual");
		goto nope;
	reblue:
		HSAC A 0{
			invoker.weaponstatus[0]&=~HMPF_JUSTUNLOAD;
			bool nomags=HDMagAmmo.NothingLoaded(self,"BluePotion");
			if(
				invoker.weaponstatus[HMPS_BLUE]>=12
				&&invoker.weaponstatus[0]&HMPF_BOTTLE
				)setweaponstate("reload");
			else if(
				invoker.weaponstatus[HMPS_BLUE]<1
				&&nomags
			)setweaponstate("reload");
		}goto unblue;
	unblue:
		HSAC A 1 A_SetCrosshair(21);
		#### H 1 ;
		#### I 2 ;
		#### J 3 A_StartSound("potion/open",8,CHANF_OVERLAP);
		---- J 0{
			int pmg=invoker.weaponstatus[HMPS_BLUE];
			bool beq=invoker.weaponstatus[0]&HMPF_BOTTLE;
			invoker.weaponstatus[HMPS_BLUE]=-1;
			invoker.weaponstatus[0]&=~HMPF_BOTTLE;
			A_StartSound("potion/swish",8);
			A_SetHelpText();
			if(pmg<1&&beq){
				A_SpawnItemEx("SpentBottle",
					cos(pitch)*12,0,height-9-sin(pitch)*12,
					vel.x,vel.y,vel.z,
					0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				setweaponstate("blueout");
				}
			else if(
				(!PressingUnload()&&!PressingReload())
				||A_JumpIfInventory("BluePotion",0,"null")
			){
				HDMagAmmo.SpawnMag(self,"BluePotion",pmg);
				setweaponstate("blueout");
			}
			else{
				HDMagAmmo.GiveMag(self,"BluePotion",pmg);
				A_StartSound("weapons/pocket",9);
				setweaponstate("pocketblue");
			}
		}
	pocketblue:
		#### JJK 5 A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		goto blueout;
	blueout:
		#### J 0{
			bool unl=invoker.weaponstatus[0]&HMPF_JUSTUNLOAD;
			if(!unl)setweaponstate("loadblue");
			}
		#### JIH 5 A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		---- A 3;
		goto reloadend;
	loadblue:
		#### K 4 A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		#### K 0 A_StartSound("weapons/pocket",9);
		#### JIH 5 A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		---- A 3;
		---- A 0{
			let mmm=hdmagammo(findinventory("BluePotion"));
			if(mmm){
				invoker.weaponstatus[HMPS_BLUE]=mmm.TakeMag(true);
				invoker.weaponstatus[0]|=HMPF_BOTTLE;
				A_SetHelpText();
				A_StartSound("potion/swish",8);
				A_StartSound("potion/open",8);
			}
		}goto reloadend;
	altreload:
	swappistols:
		---- A 0 A_SwapHandguns();
		---- A 0{
			bool id=(Wads.CheckNumForName("id",0)!=-1);
			bool offhand=invoker.wronghand;
			bool lefthanded=(id!=offhand);
			if(lefthanded){
				A_Overlay(1025,"raiseleft");
				A_Overlay(1026,"lowerright");
			}else{
				A_Overlay(1025,"raiseright");
				A_Overlay(1026,"lowerleft");
			}
		}
		TNT1 A 5;
		HSA2 A 0 A_CheckPistolHand();
		goto nope;
	lowerleft:
		HSA2 A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		HSA2 A 0;
		#### E 1 offset(-6,38);
		#### E 1 offset(-12,48);
		#### E 1 offset(-20,60);
		#### E 1 offset(-34,76);
		#### E 1 offset(-50,86);
		stop;
	lowerright:
		HSA2 A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		HSA2 A 0;
		#### E 1 offset(6,38);
		#### E 1 offset(12,48);
		#### E 1 offset(20,60);
		#### E 1 offset(34,76);
		#### E 1 offset(50,86);
		stop;
	raiseleft:
		HSA2 A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		#### A 1 offset(-50,86);
		#### A 1 offset(-34,76);
		#### A 1 offset(-20,60);
		#### A 1 offset(-12,48);
		#### A 1 offset(-6,38);
		stop;
	raiseright:
		HSA2 A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		HSA2 A 0;
		#### A 1 offset(50,86);
		#### A 1 offset(34,76);
		#### A 1 offset(20,60);
		#### A 1 offset(12,48);
		#### A 1 offset(6,38);
		stop;
	whyareyousmiling:
		#### E 1 offset(0,48);
		#### E 1 offset(0,60);
		#### E 1 offset(0,76);
		TNT1 A 7;
		HSA2 A 0{
			invoker.wronghand=!invoker.wronghand;
			A_CheckPistolHand();
		}
		#### E 1 offset(0,76);
		#### E 1 offset(0,60);
		#### E 1 offset(0,48);
		goto nope;
	spawn:
		HMPT AB -1 nodelay{
		if(invoker.weaponstatus[0]&HMPF_BOTTLE)frame=1;
		else frame=0;
		}stop;
	}
	override void initializewepstats(bool idfa){
		weaponstatus[HMPS_BLUE]=12;
		weaponstatus[HMPS_MAG]=12;
		weaponstatus[HMPS_CHAMBER]=2;
		weaponstatus[0]|=HMPF_BOTTLE;
	}
	override void loadoutconfigure(string input){
		if(weaponstatus[HMPS_BLUE]>0)weaponstatus[0]|=HMPF_BOTTLE;
	}
}
enum helzingstatus{
	HMPF_BLUEUSE=1,
	HMPF_JUSTUNLOAD=2,
	HMPF_BOTTLE=4,

	HMPS_FLAGS=0,
	HMPS_MAG=1,
	HMPS_CHAMBER=2, //0 empty, 1 spent, 2 loaded
	HMPS_BLUE=3,
};

class HelzingRandom:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let zzz=HDHelzing(spawn("HDHelzing",pos,ALLOW_REPLACE));
			if(!zzz)return;
			zzz.special=special;
			for(int i=0;i<5;i++)zzz.args[i]=args[i];
			if(random(0,5)){
				zzz.weaponstatus[0]&=~HMPF_BOTTLE;
				zzz.weaponstatus[HMPS_BLUE]=0;
			}
			spawn("HD355Mag",pos+(8,0,0),ALLOW_REPLACE);
			spawn("HD355Mag",pos+(5,0,0),ALLOW_REPLACE);
			spawn("HD355Mag",pos+(3,0,0),ALLOW_REPLACE);
		}stop;
	}
}